#include <stdio.h>
#include <stdlib.h>

typedef struct Tree
{
	struct Tree * left;
	struct Tree * right;
	int data;
} Tree;

Tree * addNode(int data)
{
	Tree * tree = (Tree *)malloc(sizeof(tree));
	tree->data = data;
	tree->left = NULL;
	tree->right = NULL;
	return tree;
}

int maxTree(Tree * tree)
{
	if (NULL == tree)
	{
		return 0;
	}
	if (NULL == tree->left && NULL == tree->right)
	{
		return 1;
	}
	return maxTree(tree->left) + maxTree(tree->right);
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0;
	int count = -1;
	int shift = 0;
	int * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	//TEST TREE
	Tree * tree = addNode(12);
	tree->left = addNode(55);
	tree->right = addNode(35);
	tree->right->left = addNode(88);
	tree->left->left = addNode(63);
	tree->right->right = addNode(28);
	tree->right->right->left = addNode(97);
	
	fprintf(output, "%d", maxTree(tree));

	return 0;
}