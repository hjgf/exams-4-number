#include <stdio.h>
#include <stdlib.h>
#include <math.h>


float gamma(float g, float * table)
{
	if (g <= 2 && g >= 1)
	{
		return table[(int)(g * 100 - 99)];
	}
	return gamma(g - 1, table) * g;
}  

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	float table[102] = {0.5, 0.2, 0.4};
	output = fopen("output.txt", "w");

	//Test
	table[26] = 0.7;
	fprintf(output, "%f", gamma(3.25, table));
	return 0;
}