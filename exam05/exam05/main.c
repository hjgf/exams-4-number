#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int toNum(char ch)
{
	if (ch >= '0' && ch <= '9')
		return ch - '0';
	return ch - 'A' + 10;
}

char toAlpha(int num)
{
	if (num >= 0 && num <=9)
		return (char)(num + '0');
	return (char)(num + 'A' - 10);
}

int anyToDec(char * s, int base)
{
	int i = 0;
	int num = 0;
	int m = 1;
	for (i = strlen(s) - 1; i >= 0; i--)
	{
		num += toNum(s[i]) * m;
		m *= base;
	}
	return num;
}

void swap(char * a, char * b)
{
	char tmp = *a;
	*a = *b;
	*b = tmp;
}

char * decToAny(int num, int base)
{
	char * str = NULL;
	int i = 0;
	int tmp = 0;
	str = (char *)malloc(256 * sizeof(char));
	do
	{
		tmp = num % base;
		num /= base;
		str[i] = toAlpha(tmp);
		i++;
	} while (0 != num);
	str[i] = 0;
	tmp = --i;
	for (; i > tmp / 2; i--)
	{
		swap(&str[tmp - i], &str[i]);
	}
	return str;
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	char s[] = "2";
	int base = 16;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fprintf(output, "%d\n", anyToDec(s, base));
	fprintf(output, "%s", decToAny(anyToDec(s, base), 2));
	return 0;
}