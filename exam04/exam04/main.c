#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

const char letter[] = "bcdfghklmnpqrstvwxz";

int check(char ch) //���������� ����� ��������� �����
{
	int i = 0;
	for (i = 0; i < 19; i++)
	{
		if (ch == letter[i])
		{
			return i;
		}
	}
	return -1;
}

int main()
{
	FILE * output;
	int i = 0, j = 0;
	char s[] = "the mistakes i made along the way made me who i am today.";
	int entry[19] = { 0 }; // 0 - ��� ���������, 1 - ��������� � ���� �����

	output = fopen("output.txt", "w");

	while ('.' != s[i - 1])
	{
		int wEntry[19] = { 0 }; //��������� � �����
		j = 0;
		while (' ' != s[i + j] && '.' != s[i + j])
		{
			int ch = check(s[i + j]);  //����� ���������
			if (-1 != ch)
			{
				wEntry[ch]++;
			}
			j++;
		}
		i = i + j + 1;
		for (j = 0; j < 19; j++) //���������� ��������� �� ����� � ����� ������
		{
			if (wEntry[j])
			{
				entry[j]++;
			}
		}
	}

	for (i = 0; i < 19; i++)
	{
		if (entry[i] == 1)
		{
			fprintf(output, "%c ", letter[i]);
		}
	}

	return 0;
}