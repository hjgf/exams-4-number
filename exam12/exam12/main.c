#include <stdio.h>
#include <stdlib.h>


int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int min = 0;
	int count = 1;
	int num;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &min);
	while (1 == fscanf(input, "%d", &num))
	{
		if (num < min)
		{
			min = num;
			count = 1;
			continue;
		}
		if (num == min)
			count++;
	}
	fprintf(output, "%d", count);
	return 0;
}