#include <stdio.h>
#include <stdlib.h>

typedef struct List
{
	int data;
	struct List * next;
} List;


List * createList(int data)
{
	List * l = (List *)malloc(sizeof(List *));
	l->data = data;
	l->next = NULL;
	return l;
}

List * addToList(List * root, int data)
{
	List * list = createList(data);
	list->next = root;
	return list;
}

/* ��� ������ */
void printList(List ** list, FILE * output)
{
	while (NULL != *list)
	{
		fprintf(output, "%d ->\n", (*list)->data);
		list = &((*list)->next);
	}
	fprintf(output, "X");
}



void unite(List ** a, List ** b, List **c)
{
	while (NULL != *a && NULL != *b)
	{
		while ((NULL != *a && NULL != *b && (*a)->data >= (*b)->data))
		{
			*c = addToList(*c, (*a)->data);
			a = &((*a)->next);
		}
		*c = addToList(*c, (*b)->data);
		b = &((*b)->next);
	}
	if (NULL == *a)
	{
		while (NULL != *b)
		{
			*c = addToList(*c, (*b)->data);
			b = &((*b)->next);
		}
	}
	while (NULL != *a)
	{
		*c = addToList(*c, (*a)->data);
		a = &((*a)->next);
	}
}

int main()
{
	FILE * input;
	FILE * output;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	List * a = createList(12);
	a->next = createList(7);
	a->next->next = createList(4);
	a->next->next->next = createList(3);
	List * b = createList(7);
	b->next = createList(2);
	b->next->next = createList(0);
	List * l = NULL;
	unite(&a, &b, &l);
	printList(&l, output);
	return 0;
}