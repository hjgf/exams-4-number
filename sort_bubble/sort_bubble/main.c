#include <stdio.h>
#include <stdlib.h>

void swap(int * a, int * b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void bubble_sort(int * arr, int count)
{
	int i = 1;
	int j = 0;
	int tmp = 0;
	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int count = -1;
	int * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &count);
	arr = (int *)malloc(count * sizeof(int));
	for (i = 0; i < count; i++)
	{
		fscanf(input, "%d ", &arr[i]);
	}
	bubble_sort(arr, count);
	for (i = 0; i < count; i++)
	{
		fprintf(output, "%d ", arr[i]);
	}

	return 0;
}