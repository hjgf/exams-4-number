#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main()
{
	FILE * input;
	FILE * output;
	int i = 0;
	int num = 0;
	int count = 0;
	int maxNum = 0;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	while (fscanf(input, "%d", &num) == 1)
	{
		int tmpCount = 0;
		for (i = 0; i < sizeof(int) * 8; i++)
		{
			if ((num >> i) & 1)
			{
				tmpCount++;
			}
		}
		if (count < tmpCount)
		{
			count = tmpCount;
			maxNum = num;
		}
	}

	fprintf(output, "%d", maxNum);
	return 0;
}