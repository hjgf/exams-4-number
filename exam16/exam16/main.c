#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int b[15] = {  0 };
	int a[15][20] = { 0 };
	int flag = 0;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	a[0][0] = 1; //test

	for (i = 0; i < 15; i++)
	{
		flag = 1;
		for (j = 0; j < 10; j++)
		{
			if (a[i][j] != a[i][19 - j])
			{
				flag = 0;
				break;
			}
		}
		b[i] = flag;
	}

	for (i = 0; i < 15; i++)
	{
		fprintf(output, "%d\n", b[i]);
	}
	return 0;
}