#include <stdio.h>
#include <stdlib.h>

void swap(int * a, int * b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void selection_sort(int * arr, int count)
{
	int i = 1;
	int j = 0;
	int tmp = 0;
	for (i = 0; i < count; i++)
	{
		int min = arr[i];
		int index = i;
		for (j = i; j < count; j++)
		{
			if (arr[j] < min)
			{
				min = arr[j];
				index = j;
			}
		}
		swap(&arr[index], &arr[i]);
	}
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int count = -1;
	int * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &count);
	arr = (int *)malloc(count * sizeof(int));
	for (i = 0; i < count; i++)
	{
		fscanf(input, "%d ", &arr[i]);
	}
	selection_sort(arr, count);
	for (i = 0; i < count; i++)
	{
		fprintf(output, "%d ", arr[i]);
	}

	return 0;
}