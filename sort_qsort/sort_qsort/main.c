#include <stdio.h>
#include <stdlib.h>

void quicksort(int * arr, int left, int right) {
	int i = left, j = right;
	int tmp;
	int pivot = arr[(left + right) / 2];

	while (i <= j) {
		while (arr[i] < pivot)
			i++;
		while (arr[j] > pivot)
			j--;
		if (i <= j) {
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			i++;
			j--;
		}
	};
	if (left < j)
		quicksort(arr, left, j);
	if (i < right)
		quicksort(arr, i, right);

}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int count = -1;
	int * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &count);
	arr = (int *)malloc(count * sizeof(int));
	for (i = 0; i < count; i++)
	{
		fscanf(input, "%d ", &arr[i]);
	}
	quicksort(arr, 0, count - 1);
	for (i = 0; i < count; i++)
	{
		fprintf(output, "%d ", arr[i]);
	}
	return 0;
}