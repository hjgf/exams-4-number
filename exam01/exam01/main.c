#include <stdio.h>

typedef struct node
{
	struct node* next;
	int content;
} node;

node* addNode(int content)
{
	node* retval = (node*)malloc(sizeof(node));
	if (NULL == retval)
	{
		return NULL;
	}
	retval->content = content;
	retval->next = NULL;
	return retval;
}

void makeList(node** head)
{
	int num;
	if ((*head) == NULL)
	{
		if (1 == scanf("%d", &num))
		{
			(*head) = addNode(num);
		}
	}	
	node* tmp = (*head);
	node* next;
	while (1 == scanf("%d", &num))
	{
		next = addNode(num);
		tmp->next = next;
		tmp = next;
	}
	return;
}

void printList(node* head)
{
	if (NULL == head)
	{
		printf("X");
	}
	else
	{
		printf("(%d)->", head->content);
		printList(head->next);
	}
	return;
}

node* invertList(node* head)
{
	if (head == NULL)
	{
		return NULL;
	}
	node* previous = NULL;
	node* current = head;
	node* next = head->next;
	while (NULL != current)
	{
		next = current->next;
		current->next = previous;
		previous = current;
		current = next;
	}
	return previous;
}

int main()
{
	node* head = NULL;
	makeList(&head);
	printList(head);
	printf("\n============\n");
	printList(invertList(head));
	getchar();
}