#include <stdio.h>
#include <stdlib.h>

typedef struct List
{
	struct List * next;
	float data;
	int times;//��� ������ � �������� ������.
} List;

List * createList(float data)
{
	List * list = (List *)malloc(sizeof(List));
	list->next = NULL;
	list->data = data;
	list->times = 1;
	return list;
}

void listAdd(List ** list, float data)
{
	List * tmp = NULL;
	while (NULL != *list)
	{
		if ((*list)->data == data)
		{
			(*list)->times++;
			return;
		}
		list = &((*list)->next);
	}
	tmp = createList(data);
	*list = tmp;
	return;
}

void deleteNode(List ** list)
{
	List * tmp = *list;
	*list = tmp->next;
	free(tmp);
}

void printList(List ** list, FILE * output)
{
	while (NULL != *list)
	{
		fprintf(output, "%f - %d ->\n", (*list)->data, (*list)->times);
		list = &((*list)->next);
	}
	fprintf(output, "X");
}

void delRepetitive(List ** root)//������� ������������� �������� �� ������
{
	while (NULL != *root)
	{
		if ((*root)->times > 1)
		{
			deleteNode(root);
		}
		else
		{
			root = &(*root)->next;
		}
	}
}
void build(List ** l1, List ** l2, FILE * input)
{
	float num;
	while (1 == fscanf(input, "%f", &num))
	{
		if (num > 0)
		{
			listAdd(l1, num);
		}
		else
		{
			listAdd(l2, num);
		}
	}
	delRepetitive(l1);
	delRepetitive(l2);
}

int main()
{
	FILE * input;
	FILE * output;
	List * l1 = NULL;
	List * l2 = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");
	
	build(&l1, &l2, input);
	printList(&l1, output);
	fprintf(output, "\n");
	printList(&l2, output);
	return 0;
}