#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * input;
	FILE * output;
	int n = -1; 
	int m = -1;
	int ** matrix = NULL;
	int ** rez = NULL;
	int i = 0, j = 0;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d %d", &n, &m);
	matrix = (int **)malloc(n * sizeof(int *));
	for (i = 0; i < n; i++)
	{
		matrix[i] = (int *)malloc(m * sizeof(int));
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			fscanf(input, "%d", &matrix[i][j]);
		}
	}



	rez = (int **)malloc(n * sizeof(int *));
	for (i = 0; i < n; i++)
	{
		rez[i] = (int *)malloc(m * sizeof(int));
	}

	rez[n - 1][m - 1] = matrix[n - 1][m - 1];

	for (j = m - 2; j >= 0; j--) //������ �������
	{
		rez[n - 1][j] = rez[n - 1][j + 1] 
			* matrix[n - 1][j]; 
	}

	for (i = n - 2; i >= 0; i--) //������ �������
	{
		rez[i][m - 1] = rez[i + 1][m - 1] * matrix[i][m - 1];
	}

	for (i = n - 2; i >= 0; i--)//����� ������
	{
		for (j = m - 2; j >= 0; j--)
		{
			rez[i][j] = matrix[i][j] * rez[i + 1][j] * rez[i][j + 1] / rez[i + 1][j + 1];
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			fprintf(output, "%.6d ", rez[i][j]);
		}
		fprintf(output, "\n");
	}
	return 0;
}