#include <stdio.h>
#include <stdlib.h>
#include <float.h>

typedef struct Tree
{
	struct Tree * left;
	struct Tree * right;
	float data;
} Tree;

Tree * createTree(float data)
{
	Tree * tree = (Tree *)malloc(sizeof(Tree));
	tree->left = NULL;
	tree->right = NULL;
	tree->data = data;
	return tree;
}

/* For debug */
void printTree(Tree * tree, FILE * output)
{
	if (NULL == tree)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "(%d ", tree->data);
	printTree(tree->left, output);
	fprintf(output, ",");
	printTree(tree->right, output);
	fprintf(output, ")");
	return;
}

int getMax(Tree * tree)
{
	int l = FLT_MIN, r = FLT_MIN;
	if (NULL == tree->left && NULL == tree->right)
	{
		return tree->data;
	}
	if (NULL != tree->left)
	{
		l = getMax(tree->left);
	}
	if (NULL != tree->right)
	{
		r = getMax(tree->right);
	}
	return max(r, l);
}

int getMin(Tree * tree)
{
	int l = FLT_MAX, r = FLT_MAX;
	if (NULL == tree->left && NULL == tree->right)
	{
		return tree->data;
	}
	if (NULL != tree->left)
	{
		l = getMax(tree->left);
	}
	if (NULL != tree->right)
	{
		r = getMax(tree->right);
	}
	return min(min(r, l), tree->data);
}

int main()
{
	FILE * input;
	FILE * output;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");
	//TEST TREE
	Tree * tree = createTree(12);
	tree->left = createTree(55);
	tree->right = createTree(35);
	tree->right->left = createTree(88);
	tree->left->left = createTree(63);
	tree->right->right = createTree(28);
	tree->right->right->left = createTree(97);
	printTree(tree, output);
	
	fprintf(output, "\n%d", (getMax(tree) - getMin(tree)));
	return 0;
}