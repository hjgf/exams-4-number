#include <stdio.h>
#include <stdlib.h>

int isExpression(char * string)
{
	int flag = 1;
	while (*string != '\0')
	{
		if (*string >= 'a' && *string <= 'z')
		{
			if (!(*(string + 1) >= 'a' && *(string + 1) <= 'z' || EOF == *(string + 1))) //������ ���� ��������� �����
			{
				string++;
				continue;
			}
			flag = 0;
		}
		if (!(*string >= '0' && *string <= '9' || *string >= 'a' &&
			*string <= 'z' || *string == '+' || *string == '-'))
			flag = 0;
		string++;
	}
	return flag;
}

int main()
{
	FILE * output;
	char str[30] = "1+2a-c";
	output = fopen("output.txt", "w");
	fprintf(output, "%d", isExpression(str));

	return 0;
}