#include <stdio.h>
#include <stdlib.h>

//�� ��� ���

int gcd(int x, int y) 
{
	while (y != 0) 
	{
		int r = x % y;
		x = y; 
		y = r;
	}
	return x;
}

void shiftk(int *a, int n, int k) 
{
	k %= n;
	if (k == 0)
		return;
	int i = 0;  // ������ ������� �������� ������� ������
	int d = gcd(n, k); // ����� ����� = ���(n, k)
	while (i < d) // ���� ��� ������ ������
	{    
		//   i - ������ ������� ������
		int j = i - k;   //   j - ��������� ������� ������
		if (j < 0)
			j += n;
		int x = a[j];  // �������� �������� ���������� ��������

		while (j != i)  // ���� ��� ������� �������� j ������
		{
			int l = j - k; //   l - ���������� ������� � j
			if (l < 0)
				l += n;
			a[j] = a[l]; //   �������� ���������� ������� � �������
			j = l;       //   ��������� � ����������� ��������
		}
		a[i] = x; // �������� �������� ���������� �������� � ������
		++i;
	}
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0;
	int count = -1;
	int shift = 0;
	int * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d %d", &count, &shift);
	arr = (int*)malloc(count * sizeof(int));
	for (i = 0; i < count; i++)
	{
		fscanf(input, "%d", &arr[i]);
	}

	shiftk(arr, count, shift);

	for (i = 0; i < count; i++)
	{
		fprintf(output, "%d ", arr[i]);
	}
	return 0;
}