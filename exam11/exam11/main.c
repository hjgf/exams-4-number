#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct Tree
{
	struct Tree * left;
	struct Tree * right;
	int data;
} Tree;

Tree * addNode(int data)
{
	Tree * tree = (Tree *)malloc(sizeof(tree));
	tree->data = data;
	tree->left = NULL;
	tree->right = NULL;
	return tree;
}

int getMax(Tree * tree)
{
	if (NULL == tree)
	{
		return INT_MIN;
	}
	return max(max(getMax(tree->left), getMax(tree->right)), tree->data);
}

int main()
{
	FILE * input;
	FILE * output;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	//TEST TREE
	Tree * tree = addNode(12);
	tree->left = addNode(55);
	tree->right = addNode(35);
	tree->right->left = addNode(88);
	tree->left->left = addNode(63);
	tree->right->right = addNode(28);
	tree->right->right->left = addNode(97);

	//fprintf(output, "%d", getMax(tree));

	float a = 0;
	fprintf(output, "%f", (0 / a));

	return 0;
}