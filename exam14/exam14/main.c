#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct Det
{
	int code;
	char name[10];
	int count;
} Det ;

//��� ��������
Det * createDet(int code, char * name, int count)
{
	Det * det = (Det *)malloc(sizeof(Det));
	det->code = code;
	det->count = count;
	strcpy(det->name, name);
	return det;
}

int main()
{
	FILE * order;
	FILE * storage;
	FILE * output;
	Det * reqDet = NULL;
	Det * storDet = NULL;
	int i = 0, j = 0;
	int flag = 1;
	output = fopen("output.txt", "w");

	/*storage = fopen("storage.txt", "w");
	order = fopen("order.txt", "w");

	fwrite(createDet(12, "Q", 15), sizeof(Det), 1, storage);
	fwrite(createDet(14, "W", 34), sizeof(Det), 1, storage);
	fwrite(createDet(15, "E", 45), sizeof(Det), 1, storage);
	fwrite(createDet(16, "R", 36), sizeof(Det), 1, storage);
	fwrite(createDet(18, "S", 37), sizeof(Det), 1, storage);
	fwrite(createDet(19, "T", 11), sizeof(Det), 1, storage);
	fwrite(createDet(24, "Y", 1), sizeof(Det), 1, storage);
	fwrite(createDet(25, "U", 34), sizeof(Det), 1, storage);
	fwrite(createDet(29, "I", 2), sizeof(Det), 1, storage);

	fwrite(createDet(12, "Q", 64), sizeof(Det), 1, order);
	fwrite(createDet(14, "W", 45), sizeof(Det), 1, order);
	fwrite(createDet(16, "R", 15), sizeof(Det), 1, order);
	fwrite(createDet(18, "S", 15), sizeof(Det), 1, order);
	fwrite(createDet(20, "O", 14), sizeof(Det), 1, order);
	fwrite(createDet(25, "U", 15), sizeof(Det), 1, order);
	fwrite(createDet(29, "I", 15), sizeof(Det), 1, order);
	fwrite(createDet(34, "P", 15), sizeof(Det), 1, order);*/

	storage = fopen("storage.txt", "r");
	order = fopen("order.txt", "r");
	reqDet = (Det *)malloc(sizeof(Det));
	storDet = (Det *)malloc(sizeof(Det));
	fread(storDet, sizeof(Det), 1, storage);
	while (1 == fread(reqDet, sizeof(Det), 1, order))
	{
		int flag = 1;
		if (reqDet->code < storDet->code)
		{
			continue;
		}
		if (reqDet->code == storDet->code)
		{
			if (storDet->count < reqDet->count)
			{
				fprintf(output, "Not enough %d %s with %d code\n",
					reqDet->count - storDet->count, reqDet->name, reqDet->code);
			}
			continue;
		}
		while (1 == fread(storDet, sizeof(Det), 1, storage)
			&& storDet->code <= reqDet->code) //�����, ����������
		{
			if (storDet->code == reqDet->code)
			{
				if (storDet->count < reqDet->count)
				{
					fprintf(output, "Not enough %d %s with %d code\n",
						reqDet->count - storDet->count, reqDet->name, reqDet->code);
				}
			}
			flag = 0;
		}
		if (!flag)
		{
			continue;
		}
		fprintf(output, "Not enough %d %s with %d code\n",
			reqDet->count, reqDet->name, reqDet->code);
	}
	return 0;
}