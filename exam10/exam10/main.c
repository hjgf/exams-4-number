#include <stdio.h>
#include <stdlib.h>

void dfs(int ** matrix, int * passed, int count, int current)
{
	int i = 0;
	passed[current] = 1;
	for (i = 0; i < count; i++)
	{
		if (matrix[current][i] && !passed[i])
		{
			dfs(matrix, passed, count, current + 1);
		}
	}
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int count = 1;
	int ** matrix = NULL;
	int * passed = NULL;
	int vert = -1;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &count);
	fscanf(input, "%d", &vert);
	passed = (int *)calloc(count, sizeof(int));
	matrix = (int **)calloc(count, sizeof(int *));
	for (i = 0; i < count; i++)
	{
		matrix[i] = (int **)malloc(count * sizeof(int *));
	}

	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count; j++)
		{
			fscanf(input, "%d", &matrix[i][j]);
		}
	}

	dfs(matrix, passed, count, vert);

	for (i = 0; i < count; i++)
	{
		if (passed[i])
		{
			fprintf(output, "%d ", i);
		}
	}
	return 0;
}