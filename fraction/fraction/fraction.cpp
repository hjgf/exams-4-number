#include<iostream>
#include"fraction.h"

inline void Fraction::setNumerator(int _num)
{
	numerator = _num;
}

inline void Fraction::setDenominator(int _denom)
{
	denominator = _denom; //���� ���� ���������� ��� ������� �� ����!
}

inline void Fraction::set(int _num, int _denom)
{
	setDenominator(_denom);
	setNumerator(_num);
}

Fraction::Fraction(int _num, int _denom)
{
	setNumerator(_num);
	setDenominator(_denom);
}

inline Fraction Fraction::operator * (const Fraction & _f)
{
	return multiply(_f);
}

inline Fraction Fraction::operator + (const Fraction & _f)
{
	return add(_f);
}

inline Fraction Fraction::operator / (const Fraction & _f)
{
	return division(_f);
}

inline Fraction Fraction::operator - (const Fraction & _f)
{
	return subtraction(_f);
}

bool Fraction::operator == (const Fraction & _f)
{
	if (getDenominator() * getNumerator() == _f.getDenominator() * _f.getNumerator()
		&& abs(getDenominator()) == abs(_f.getDenominator())
		&& abs(getNumerator()) == abs(_f.getNumerator()))
	{
		return true;
	}
	return false;
}

Fraction Fraction::multiply(const Fraction & _f)
{
	Fraction tmp(getNumerator()	* _f.getNumerator(),
		getDenominator() * _f.getDenominator());
	normalyze();
	return tmp;
}

Fraction Fraction::subtraction(const Fraction & _f)
{
	int newDenom = lcm(getDenominator(), _f.getDenominator());
	return Fraction(getNumerator()	* newDenom / getDenominator() -
		_f.getNumerator()	* newDenom / _f.getDenominator(), newDenom);
}

Fraction Fraction::division(const Fraction & _f)
{
	return Fraction(getNumerator()	* _f.getDenominator(),
		getDenominator() * _f.getNumerator()); //���������� ��� ������� �� ����!
}

Fraction Fraction::add(const Fraction & _f)
{
	int newDenom = lcm(getDenominator(), _f.getDenominator());
	return Fraction(getNumerator()	* newDenom / getDenominator() + 
		_f.getNumerator()	* newDenom / _f.getDenominator(), newDenom);
}

void Fraction::normalyze()
{
	int _gcd = gcd(getDenominator(), getNumerator());
	setDenominator(getDenominator() / _gcd);
}

void Fraction::print() const
{
	std::cout << "(" << numerator << " / " << denominator << ")" << std::endl;
}

void Fraction::signedPrint() const
{
	if (denominator * numerator < 0)
	{
		std::cout << "-";
	}
	std::cout << "(" << abs(numerator) << " / " << abs(denominator) << ")" << std::endl;
}

int gcd(int x, int y)
{
	while (y != 0)
	{
		int r = x % y;
		x = y;
		y = r;
	}
	return x;
}

int lcm(int x, int y)
{
	return (x * y) / gcd(x, y);
}