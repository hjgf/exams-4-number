#ifndef FRACTION_H
#define FRACTION_H

class Fraction
{
private:
	int numerator;
	int denominator;
public:
	Fraction(int, int);
	void setNumerator(int);
	void setDenominator(int);
	void set(int numerator, int denominator);
	int getNumerator() const { return numerator; };
	int getDenominator() const { return denominator; };
	Fraction multiply(const Fraction &);
	Fraction operator * (const Fraction &);
	Fraction add(const Fraction &);
	Fraction operator + (const Fraction &);
	Fraction division(const Fraction &);
	Fraction operator / (const Fraction &);
	Fraction subtraction(const Fraction &);
	Fraction operator - (const Fraction &);
	void normalyze();
	bool operator == (const Fraction &);
	void print() const;
	void signedPrint() const;
};

int gcd(int x, int y);
int lcm(int x, int y);

#endif