#include <stdio.h>

int shiftLetter(char letter, char shift)
{
	return (letter - 'a' + shift) % 25 + 'a';
}

void cryptFile(FILE* input, FILE* output, int shift)
{
	int ch;
	while (EOF != (ch = fgetc(input)))
	{
		if (ch <= 'z' && ch >= 'a')
		{
			fputc(shiftLetter((char)ch, shift), output);
		}
		else
		{
			fputc(ch, output);
		}
	}
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr,"Error opening input file");
		return 1;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr,"Error opening output file");
		return 1;
	}

	int shift;
	printf("Enter shift, please!\n");
	scanf("%d",&shift);

	cryptFile(input, output, shift);

	fclose(input);
	fclose(output);
	return;
}