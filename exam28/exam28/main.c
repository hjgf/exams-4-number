#include <stdio.h>
#include <stdlib.h>

typedef struct List
{
	struct List * next;
	int vert;
} List;

List * createList(int vert)
{
	List * list = (List *)malloc(sizeof(List));
	list->next = NULL;
	list->vert = vert;
	return list;
}

void addToList(List ** root, int vert)
{
	List * list = NULL;
	while (NULL != *root)
	{
		root = &((*root)->next);
	}
	list = createList(vert);
	*root = list;
}

/* ��� ������ */
void printList(List ** list, FILE * output)
{
	while (NULL != *list)
	{
		
		fprintf(output, "%d->", (*list)->vert);
		list = &((*list)->next);
	}
	fprintf(output, "X");
}

int main()
{
	FILE * input;
	FILE * output;
	int i = 0;
	int count = -1;
	int arc = -1;
	List ** arr = NULL;
	int v1, v2;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d %d", &count, &arc);
	arr = (List**)calloc(count, sizeof(List));
	for (i = 0; i < arc; i++)
	{
		fscanf(input, "%d %d", &v1, &v2);
		addToList(&arr[v1], v2);
		addToList(&arr[v2], v1);
	}

	for (i = 0; i < count; i++)
	{
		printList(&arr[i], output);
		fprintf(output, "\n");
	}
	return 0;
}