#include <stdio.h>
#include <stdlib.h>

int calc(char a, char op, char b)
{
	switch (op)
	{
	case '+': return a + b;
	case '-': return a - b;
	case '*': return a * b;
	}
}

int calcExpression(FILE * input)
{
	int tmp;
	int l;
	char op;
	char r;
	if ('(' != (tmp = fgetc(input))) //������, ����� ������ �����
	{
		return tmp;
	}
	if ('(' == (tmp = fgetc(input)))
	{
		ungetc(tmp, input);
		l = calcExpression(input);
	}
	else
	{
		l = tmp - '0';
	}
	op = fgetc(input);
	r = fgetc(input);
	fgetc(input);
	return calc(l, op, r - '0');
}



int main()
{
	FILE * input;
	FILE * output;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fprintf(output, "%d",calcExpression(input));

	return 0;
}