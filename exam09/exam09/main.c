#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int isDivider(char ch)
{
	if (' ' == ch || ',' == ch || '.' == ch)
		return 1;
	return 0;
}

int main()
{
	FILE * output;
	int i = 0, j = 0;
	char s[] = "the mistakes i made along the way made me who i am today.";
	int length[50] = { 0 };
	output = fopen("output.txt", "w");
	for (i = 0; i < strlen(s); i++)
	{
		j = 0;
		while (!isDivider(s[i + j]) && i + j < strlen(s))
		{
			j++;
		}
		i += j;
		length[j]++;
	}
	for (i = 1; i < 50; i++)
	{
		fprintf(output, "l = %d count = %d\n", i, length[i]);
	}

	return 0;
}