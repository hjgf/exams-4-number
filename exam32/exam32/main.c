#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * input;
	FILE * output;
	int i = 0, j = 0;
	int count = -1;
	int prev = 0;
	int curr = 0;
	int tmpCnt = 0;
	int maxCnt = 0;
	int maxNum = 0;
	char * arr = NULL;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	fscanf(input, "%d", &count);
	arr = (char *)malloc(count * sizeof(char));
	for (i = 0; i < count; i++)
	{
		fscanf(input, "%c", &arr[i]);
	}

	for (i = 0; i < count; i++)
	{
		tmpCnt = 0;
		for (j = 0; j < 8; j++)
		{
			curr = (arr[i] >> j) & 1;
			if (curr && prev)
			{
				tmpCnt++;
			}
			prev = curr;
		}
		if (tmpCnt > maxCnt)
		{
			maxCnt = tmpCnt;
			maxNum = arr[i];
		}
	}

	fprintf(output, "%d", maxCnt);
	return 0;
}