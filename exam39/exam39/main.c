#include <stdio.h>
#include <stdlib.h>

typedef struct s_tree
{
	struct s_tree * left;
	struct s_tree * right;
	int data;
} s_tree;

s_tree * addNode(int data)
{
	s_tree * tree = (s_tree *)malloc(sizeof(tree));
	tree->data = data;
	tree->left = NULL;
	tree->right = NULL;
	return tree;
}

void printTree(s_tree * tree, FILE * output)
{
	if (NULL == tree)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "(%d ", tree->data);
	printTree(tree->left, output);
	fprintf(output, ",");
	printTree(tree->right, output);
	fprintf(output, ")");
}

int getCountOnLevel(s_tree * tree, int level, int current, int * count)
{
	if (NULL == tree)
	{
		return;
	}
	if (current == level)
	{
		(*count)++;
		return;
	}
	getCountOnLevel(tree->left, level, current + 1, count);
	getCountOnLevel(tree->right, level, current + 1, count);
}

int main()
{
	int count = -1;
	FILE * input;
	FILE * output;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	//TEST TREE
	s_tree * tree = addNode(12);
	tree->left = addNode(55);
	tree->right = addNode(35);
	tree->right->left = addNode(88);
	tree->left->left = addNode(63);
	tree->right->right = addNode(28);
	tree->right->right->left = addNode(97);
	printTree(tree, output);
	getCountOnLevel(tree, 4, 1, &count);
	fprintf(output, "\n%d", count);
}