#include <stdio.h>
#include <stdlib.h>

typedef struct Tree
{
	struct Tree * left;
	struct Tree * right;
	char ch;
} Tree;

Tree * createTree(char ch)
{
	Tree * tree = (Tree *)malloc(sizeof(Tree));
	tree->left = NULL;
	tree->right = NULL;
	tree->ch = ch;
}

Tree * buildTree(FILE * input)
{
	int tmp;
	Tree * op = NULL;
	Tree * left = NULL;
	if ('(' != (tmp = fgetc(input))) //������, ����� ������ �����
	{
		return buildTree((char)tmp);
	}
	if ('(' == (tmp = fgetc(input)))
	{
		ungetc(tmp, input);
		left = buildTree(input);
	}
	else
	{
		left = createTree((char)tmp);
	}
	op = createTree((char)fgetc(input));
	op->right = createTree((char)fgetc(input));
	fgetc(input); // ��������� ������
	op->left = left;
	return op;
}

/* For debug */
void printTree(Tree * tree, FILE * output)
{
	if (NULL == tree)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "(%c ", tree->ch);
	printTree(tree->left, output);
	fprintf(output, ",");
	printTree(tree->right, output);
	fprintf(output, ")");
	return;
}

int main()
{
	FILE * input;
	FILE * output;

	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");
	printTree(buildTree(input), output);
	
	return 0;
}