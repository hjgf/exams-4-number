#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * input;
	FILE * output;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	/*��������� ������ � ������ �����������*/
	int dim1;
	fscanf(input, "%d", &dim1);

	int** pol1;
	pol1 = (int**)malloc(sizeof(int*)* dim1);
	for (int i = 0; i < dim1; i++)
	{
		pol1[i] = (int*)malloc(sizeof(int)* dim1);
	}

	for (int i = 0; i < dim1; i++)
	{
		for (int j = 0; j < dim1; j++)
		{
			fscanf(input, "%d", &pol1[i][j]);
		}
	}

	int dim2;
	fscanf(input, "%d", &dim2);

	int** pol2;
	pol2 = (int**)malloc(sizeof(int*)* dim2);
	for (int i = 0; i < dim2; i++)
	{
		pol2[i] = (int*)malloc(sizeof(int)* dim2);
	}

	for (int i = 0; i < dim1; i++)
	{
		for (int j = 0; j < dim1; j++)
		{
			fscanf(input, "%d", &pol2[i][j]);
		}
	}
	/*�������������� ���������*/
	int newDim = dim1 + dim2;

	int** pol;
	pol = (int**)malloc(sizeof(int*)* newDim);
	for (int i = 0; i < newDim; i++)
	{
		pol[i] = (int*)calloc(newDim, sizeof(int));
	}

	/*���������*/
	for (int i = 0; i < dim1; i++)
	{
		for (int j = 0; j < dim1; j++)
		{
			for (int k = 0; k < dim2; k++)
			{
				for (int l = 0; l < dim2; l++)
				{
					pol[i + k][j + l] += pol1[i][j] * pol1[i][j];
				}
			}
		}
	}

	/*����� �� �����*/
	for (int i = 0; i < newDim; i++)
	{
		for (int j = 0; j < newDim; j++)
		{
			fprintf(output, "%.2d ", pol[i][j]);
		}
		fprintf(output, "\n");
	}

	return 0;
}