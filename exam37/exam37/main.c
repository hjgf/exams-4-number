#include <stdio.h>
#include <stdlib.h>

typedef struct s_tree
{
	struct s_tree * left;
	struct s_tree * right;
	int data;
} s_tree;

typedef struct List
{
	struct List * next;
	struct s_tree * tree;
} List;

typedef struct Queue
{
	struct List * first;
} Queue;

Queue * createQueue()
{
	Queue * q = (Queue *)malloc(sizeof(Queue *));
	q->first = NULL;
	return q;
}

List * createList(s_tree * tree)
{
	List * l = (List *)malloc(sizeof(List *));
	l->tree = tree;
	l->next = NULL;
	return l;
}

void addToQueue(Queue * q, s_tree * tree)
{
	List * tmp = createList(tree);
	if (NULL == q->first)
	{
		q->first = tmp;
		return;
	}
	tmp->next = q->first;
	q->first = tmp;
	return;
}

s_tree * addNode(int data)
{
	s_tree * tree = (s_tree *)malloc(sizeof(tree));
	tree->data = data;
	tree->left = NULL;
	tree->right = NULL;
	return tree;
}

/* For debug */
void printTree(s_tree * tree, FILE * output)
{
	if (NULL == tree)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "(%d ", tree->data);
	printTree(tree->left, output);
	fprintf(output, ",");
	printTree(tree->right, output);
	fprintf(output, ")");
	return;
}

/* For debug */
void printList(List ** list, FILE * output)
{
	while (NULL != *list)
	{
		printTree((*list)->tree, output);
		fprintf(output, "->\n");
		list = &((*list)->next);
	}
	fprintf(output, "X");
}

int isEmpty(Queue * queue)
{
	if (NULL == queue->first)
	{
		return 1;
	}
	return 0;
}

List * qGet(Queue * q)
{
	List * tmp = q->first;
	q->first = q->first->next;
	return tmp;
}

void getLength(FILE * output, s_tree * root)
{
	List * tmp = NULL;
	Queue * queue = createQueue();
	addToQueue(queue, root);
	while (!isEmpty(queue))
	{
		tmp = qGet(queue);
		fprintf(output, "%d\n", tmp->tree->data);
		if (NULL != tmp->tree->right)
		{
			addToQueue(queue, tmp->tree->right);
		}
		if (NULL != tmp->tree->left)
		{
			addToQueue(queue, tmp->tree->left);
		}
	}
	return; //Not in tree
}

int main()
{
	FILE * input;
	FILE * output;
	input = fopen("input.txt", "r");
	input = fopen("output.txt", "w");

	//TEST TREE
	s_tree * tree = addNode(12);
	tree->left = addNode(55);
	tree->right = addNode(35);
	tree->right->left = addNode(88);
	tree->left->left = addNode(63);
	tree->right->right = addNode(28);
	tree->right->right->left = addNode(97);
	printTree(tree, output);

	fprintf(output, "\n");
	getLength(output, tree);
}