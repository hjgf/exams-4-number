#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//���������� ������� ������

#define EPS 0.004

int main()
{
	FILE * input;
	FILE * output;
	float xn1 = 1;
	float xn2 = 1;
	float num = 3;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");
	
	fprintf(stdout, "Enter number\n");
	fscanf(stdin, "%f", &num);
	do 
	{
		xn1 = xn2;
		xn2 = (xn1 + num / xn1) / 2;
	} while (fabs(xn2 - xn1) >= EPS);
	fprintf(output, "%.12f", xn2);
	return 0;
}