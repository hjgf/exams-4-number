#include <stdio.h>
#include <stdlib.h>

typedef struct Tree
{
	struct Tree * left;
	struct Tree * right;
	int data;
} Tree;

Tree * addNode(int data)
{
	Tree * tree = (Tree *)malloc(sizeof(tree));
	tree->data = data;
	tree->left = NULL;
	tree->right = NULL;
	return tree;
}

void printTree(Tree * tree, FILE * output)
{
	if (NULL == tree)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "(%d ", tree->data);
	printTree(tree->left, output);
	fprintf(output, ",");
	printTree(tree->right, output);
	fprintf(output, ")");
}

int max2(int a, int b)
{
	return a > b ? a : b;
}

int getHeight(Tree * tree)
{
	if (NULL == tree)
	{
		return 0;
	}
	return max2(getHeight(tree->left), getHeight(tree->right)) + 1;
}

int main()
{
	FILE * input;
	FILE * output;
	input = fopen("input.txt", "r");
	output = fopen("output.txt", "w");

	//TEST TREE
	Tree * tree = addNode(12);
	tree->left = addNode(55);
	tree->right = addNode(35);
	tree->right->left = addNode(88);
	tree->left->left = addNode(63);
	tree->right->right = addNode(28);
	tree->right->right->left = addNode(97);
	printTree(tree, output);

	fprintf(output, "\n%d", getHeight(tree));
}